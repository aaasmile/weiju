package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.sysadmin.pojo.dto.appeal.WjAppealPageDTO;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.sysadmin.pojo.vo.appeal.QueryAppealByIdVo;

/**
 * <p>
 * 诉求表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealService extends IService<WjAppeal> {

    /**
     * 按条件分页查询 诉求
     *
     * @param
     * @Author Zol
     * @Date 2020/08/26 15:19
     **/
    PageData listPageAppeal(WjAppealPageDTO wjAppealPageDTO);

    /**
     * 查询单个诉求详情
     *
     * @param
     * @Author Zol
     * @Date 2020/08/26 15:19
     **/
    QueryAppealByIdVo appealById(String appealId);



}
