package com.huixi.microspur.sysadmin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.sysadmin.mapper.WjAppealCommentMapper;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealComment;
import com.huixi.microspur.sysadmin.service.WjAppealCommentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-评论 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealCommentServiceImpl extends ServiceImpl<WjAppealCommentMapper, WjAppealComment> implements WjAppealCommentService {

}
