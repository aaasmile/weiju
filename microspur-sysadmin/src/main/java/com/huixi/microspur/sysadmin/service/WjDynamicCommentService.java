package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.dynamic.WjDynamicComment;

/**
 * <p>
 * 动态评论表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicCommentService extends IService<WjDynamicComment> {

}
