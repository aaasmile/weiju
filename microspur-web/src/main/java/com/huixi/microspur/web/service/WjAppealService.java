package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealPageDTO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealPageNearbyDTO;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.web.pojo.vo.appeal.QueryAppealByIdVo;
import com.huixi.microspur.web.pojo.vo.appeal.QueryAppealVO;
import com.huixi.microspur.web.pojo.vo.appeal.QueryNearbyAppealVO;

import java.util.List;

/**
 * <p>
 * 诉求表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealService extends IService<WjAppeal> {


    /**
     * 根据诉求id 查询诉求
     *
     * @param appealId 诉求id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     * @Author 叶秋
     * @Date 2020/6/3 21:24
     **/
    QueryAppealByIdVo queryByAppealId(String appealId);


    /**
     *  按条件分页查询 诉求
     * @Author 叶秋
     * @Date 2020/2/26 21:14
     * @param
     * @return java.util.List<com.huixi.microspur.web.pojo.entity.appeal.WjAppeal>
     **/
    PageData listPageAppeal(WjAppealPageDTO wjAppealPageDTO);


    /**
     *  根据经纬度查询附近的诉求
     * @Author 叶秋
     * @Date 2020/7/14 23:30
     * @param wjAppealPageNearbyDTO
     * @return java.util.List<com.huixi.microspur.web.pojo.vo.appeal.QueryNearbyAppealVO>
     **/
    List<QueryNearbyAppealVO> listPageNearbyAppeal(WjAppealPageNearbyDTO wjAppealPageNearbyDTO);



    /**
     * 根据所给的用户id 分页查询他所有的诉求（就是查询我的 诉求）
     * @Author 叶秋
     * @Date 2020/6/3 22:04
     * @param wjAppealPageVO
     * @return com.huixi.microspur.commons.page.PageData
     **/
    PageData ListByUserIdMyAppeal(WjAppealPageDTO wjAppealPageVO);


    /**
     *  判断是否是自己发布的
     * @Author 叶秋
     * @Date 2020/7/1 21:06
     * @param appealId 诉求id
     * @param userId 用户id
     * @return java.lang.Boolean
     **/
    Boolean judgeAppealIsMe(String appealId, String userId);



    /**
     *  校验于此诉求沟通的人是否已满
     * @Author 叶秋
     * @Date 2020/7/9 21:44
     * @param appealId 诉求id
     * @return java.lang.Boolean
     **/
    Boolean verifyAppealFull(String appealId);







}
