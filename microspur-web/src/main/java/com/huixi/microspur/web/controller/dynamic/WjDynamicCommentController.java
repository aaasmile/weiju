package com.huixi.microspur.web.controller.dynamic;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageFactory;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.dto.dynamic.QueryDynamicCommentVO;
import com.huixi.microspur.web.pojo.dto.dynamic.WjDynamicCommentDTO;
import com.huixi.microspur.web.pojo.dto.dynamic.WjDynamicCommentPageDTO;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicComment;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.service.WjDynamicCommentService;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 动态评论表 前端控制器
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjDynamicComment")
@Api(tags = "动态评论模块")
public class WjDynamicCommentController extends BaseController {

    @Resource
    private WjDynamicCommentService wjDynamicCommentService;

    @Resource
    private WjUserService wjUserService;


    /**
     *  添加动态评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param wjDynamicCommentDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/dynamicComment")
    @ApiOperation(value = "添加动态评论")
    public Wrapper dynamicComment(@Valid @RequestBody WjDynamicCommentDTO wjDynamicCommentDTO){

        String nowUserId = CommonUtil.getNowUserId();
        wjDynamicCommentDTO.setUserId(nowUserId);

        WjDynamicComment wjDynamicComment = new WjDynamicComment();

        BeanUtil.copyProperties(wjDynamicCommentDTO, wjDynamicComment);

        boolean save = wjDynamicCommentService.save(wjDynamicComment);

        return Wrapper.ok(save);

    }


    /**
     *  删除 动态评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param dynamicCommentId 动态id
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @DeleteMapping("/dynamicComment/{dynamicCommentId}")
    @ApiOperation(value = "删除 动态评论")
    @ApiImplicitParam(name = "dynamicCommentId", value = "动态评论的id", dataType = "String"
            ,required = true)
    public Wrapper dynamicComment(@PathVariable String dynamicCommentId){

        boolean b = wjDynamicCommentService.removeById(dynamicCommentId);

        return Wrapper.ok(b);

    }



    /**
     *  分页查询动态 评论
     * @Author 叶秋
     * @Date 2020/4/13 22:48
     * @param wjDynamicCommentPageDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/pageDynamicComment")
    @ApiOperation(value = "分页查询诉求的 评论")
    public Wrapper pageDynamicComment(@Valid @RequestBody WjDynamicCommentPageDTO wjDynamicCommentPageDTO) {

        PageData<QueryDynamicCommentVO> objectPageData = new PageData<>();
        List<QueryDynamicCommentVO> queryDynamicCommentVOS = new ArrayList<>();

        // 查询出来的评论
        Page<WjDynamicComment> wjDynamicPage = PageFactory.createPage(wjDynamicCommentPageDTO.getPageQuery());

        Page<WjDynamicComment> wjDynamicCommentPage = wjDynamicCommentService.lambdaQuery()
                .orderByDesc(WjDynamicComment::getCreateTime)
                .eq(WjDynamicComment::getDynamicId, wjDynamicCommentPageDTO.getDynamicId())
                .page(wjDynamicPage);


        // 查询出用户
        List<WjDynamicComment> records = wjDynamicCommentPage.getRecords();
        Set<String> userIds = records.stream().map(WjDynamicComment::getUserId).collect(Collectors.toSet());
        List<WjUser> userList =
                wjUserService.lambdaQuery().in(CollUtil.isNotEmpty(userIds), WjUser::getUserId, userIds).list();


        QueryDynamicCommentVO queryDynamicCommentVO;
        for (WjDynamicComment record : records) {
            queryDynamicCommentVO = new QueryDynamicCommentVO();
            BeanUtil.copyProperties(record, queryDynamicCommentVO);

            WjUser wjUser = userList.stream().filter(e -> e.getUserId().equals(record.getUserId())).findAny().orElseGet(WjUser::new);
            queryDynamicCommentVO.setNickName(wjUser.getNickName()).setHeadPortrait(wjUser.getHeadPortrait());

            queryDynamicCommentVOS.add(queryDynamicCommentVO);
        }

        BeanUtil.copyProperties(wjDynamicCommentPage, objectPageData);
        objectPageData.setRecords(queryDynamicCommentVOS);


        return Wrapper.ok(objectPageData);

    }



}

