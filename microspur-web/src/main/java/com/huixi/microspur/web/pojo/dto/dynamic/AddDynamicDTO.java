package com.huixi.microspur.web.pojo.dto.dynamic;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *  添加动态接收前端的值
 * @Author 叶秋
 * @Date 2020/7/22 14:31
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="添加动态接收前端的值")
public class AddDynamicDTO implements Serializable {

    @ApiModelProperty(value = "发送人的id", hidden = true)
    private String userId;

    @NotNull(message = "内容不可为空")
    @ApiModelProperty(value = "发送的内容（叶秋测试）")
    private String content;

    @ApiModelProperty(value = "上传的文件地址")
    private String url;


}
