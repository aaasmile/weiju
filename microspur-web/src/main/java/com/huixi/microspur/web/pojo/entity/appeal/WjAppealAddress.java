package com.huixi.microspur.web.pojo.entity.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  诉求地址表
 * @Author 叶秋 
 * @Date 2020/3/25 2:04
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_address")
@ApiModel(value="WjAppealAddress对象", description="诉求-地址")
public class WjAppealAddress extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "诉求具体位置的主键")
    @TableId(value = "appeal_address_id", type = IdType.ASSIGN_UUID)
    private String appealAddressId;

    @ApiModelProperty(value = "对应的诉求id")
    @TableField("appeal_id")
    private String appealId;

    @ApiModelProperty(value = "纬度")
    @TableField("latitude")
    private String latitude;

    @ApiModelProperty(value = "经度")
    @TableField("longitude")
    private String longitude;

    @ApiModelProperty(value = "国家")
    @TableField("nation")
    private String nation;

    @ApiModelProperty(value = "省")
    @TableField("province")
    private String province;

    @ApiModelProperty(value = "市")
    @TableField("city")
    private String city;

    @ApiModelProperty(value = "区")
    @TableField("district")
    private String district;

    @ApiModelProperty(value = "行政区划代码")
    @TableField("adcode")
    private Integer adcode;

    @ApiModelProperty(value = "详细地址")
    @TableField("detailed_address")
    private String detailedAddress;

    @ApiModelProperty(value = "备用地址(冗余)")
    @TableField("standby_address")
    private String standbyAddress;

    @ApiModelProperty(value = "备用的地址code(冗余)")
    @TableField("standby_address_code")
    private Integer standbyAddressCode;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;


}
