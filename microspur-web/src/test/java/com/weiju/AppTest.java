package com.weiju;


import com.huixi.microspur.web.WebApplication;
import com.huixi.microspur.web.service.WjAppealService;
import com.huixi.microspur.web.service.WjChatService;
import com.huixi.microspur.web.service.WjChatUserService;
import com.huixi.microspur.web.service.WjDynamicService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    WjAppealService wjAppealService;

    @Autowired
    WjDynamicService wjDynamicService;

    @Autowired
    WjChatService wjChatService;

    @Autowired
    WjChatUserService wjChatUserService;


    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {

//        stringRedisTemplate.opsForValue().set("hello","world10086");

    }


    @Test
    public void test1() {

//        QueryWrapper<com.huixi.microspur.web.pojo.entity.Test> queryWrapper = new QueryWrapper<>();
//        queryWrapper.orderByDesc("id");
//
//        IPage<com.huixi.microspur.web.pojo.entity.Test> testIPage = new Page<>(1, 10);//参数一是当前页，参数二是每页个数
//        testIPage = testService.page(testIPage, queryWrapper);
//
//        List<com.huixi.microspur.web.pojo.entity.Test> records = testIPage.getRecords();
//        for (com.huixi.microspur.web.pojo.entity.Test record : records) {
//            System.out.println(record);
//        }


    }

    /**
     * 添加 动态
     *
     * @param
     * @return void
     * @Author 叶秋
     * @Date 2020/4/19 21:32
     **/
    @Test
    public void test2() {

//        WjDynamic wjDynamic = new WjDynamic();
//
//        wjDynamic.setDynamicId(IdUtil.simpleUUID());
//        wjDynamic.setUserId("76d363f5df774f5b965aae33dbb4e536");
//        wjDynamic.setContent("这是一条测试的动态，谢谢大家的支持。我会好好加油干的");
//        wjDynamic.setUrl("https://weiju1.oss-cn-shenzhen.aliyuncs.com/xiaochengxu-readme/channels4_banner.jpg");
//        wjDynamic.setFileType("JPG");
//
//        boolean save = wjDynamicService.save(wjDynamic);


    }


    /**
     * 添加 聊天室
     *
     * @param
     * @return void
     * @Author 叶秋
     * @Date 2020/4/26 23:26
     **/
    @Test
    public void test3() {

//        WjChat wjChat = new WjChat();
//        wjChat.setChatId(IdUtil.simpleUUID());
//        wjChat.setType("one");
//        wjChat.setTitle("Hello World2");
//        wjChat.setPeopleNumber(1);
//
//        wjChatService.save(wjChat);


    }


    /**
     *  添加聊天室的用户
     * @Author 叶秋
     * @Date 2020/4/26 23:48
     * @param
     * @return void
     **/
    @Test
    public void test4(){

//        WjChatUser wjChatUser = new WjChatUser();
//
//        wjChatUser.setChatId("a8ae3ad7bd2a420f82f4c19d3832adf1");
//        wjChatUser.setUserId("c6837ade5a894e7f8d2fac512ded52dd");
//
//        wjChatUserService.save(wjChatUser);

    }


    @Test
    public void test5(){

//        String fileName = "1.jpg";
//
//        String substring = fileName.substring(fileName.lastIndexOf("."));
//
//        System.out.println(substring);




    }



    // 定一个点， 查找半径内的 点（就是一个圆），并列出距离
    @Test
    public void test6(){


//        BoundGeoOperations boundGeoOperations = redisTemplate.boundGeoOps("wjAppeal");
//
//        //定园区里面的一个点
//        Point point = new Point(113.435587, 23.162477);
//
//        // 1 千米 的距离范围内
//        Metric metric = RedisGeoCommands.DistanceUnit.METERS;
//        Distance distance = new Distance(1000, metric);
//        Circle circle = new Circle(point, distance);
//
//        // 一些设置
//        RedisGeoCommands.GeoRadiusCommandArgs args = RedisGeoCommands
//                .GeoRadiusCommandArgs.newGeoRadiusArgs()
//                // 结果包含距离
//                .includeDistance()
//                // 结果包含坐标
//                .includeCoordinates()
//                // 升序
//                .sortAscending()
//                .limit(5);
//
//        GeoResults<RedisGeoCommands.GeoLocation<String>> geoResults = redisTemplate.boundGeoOps("wjAppeal").radius(circle, args);
//        List<GeoResult<RedisGeoCommands.GeoLocation<String>>> geoResultList = geoResults.getContent();
//        for(GeoResult<RedisGeoCommands.GeoLocation<String>> geoResult:geoResultList) {
//            String name = geoResult.getContent().getName(); // 输出我定的 12138 / 12139
//            Point point2 = geoResult.getContent().getPoint(); // 这个位置的经纬度
//            double value = geoResult.getDistance().getValue(); // 距离
//            Metric metric2 = geoResult.getDistance().getMetric(); // 距离的单位 米，这个是在上面定的
//
//        }


    }



    // 添加经纬度
    @Test
    public void test7(){

//        // 广东软件园
//        Point point = new Point(113.437000, 23.162601);
//        // 丰巢
//        Point point2 = new Point(113.437019, 23.163394);
//
//        // 这个是主键 相当于key
//        BoundGeoOperations boundGeoOperations = redisTemplate.boundGeoOps("wjAppeal");
//        // 这个 12138 这个区域，不单单可以填String，它是Object类型
//        boundGeoOperations.add(point, "12138");
//        boundGeoOperations.add(point2, "12139");

    }


    // 从redis 中获取 已经加入进去的坐标
    @Test
    public void test8(){

//        BoundGeoOperations boundGeoOperations = redisTemplate.boundGeoOps("wjAppeal");
//        List<Point> position = boundGeoOperations.position("12138","12139");
//
//
//        for (Point point : position) {
//            System.out.println(point);
//        }
////         输出的结果
////        Point [x=113.437000, y=23.162600]
////        Point [x=113.437022, y=23.163393]


    }



    // 两点之间距离
    @Test
    public void test9(){

//        BoundGeoOperations boundGeoOperations = redisTemplate.boundGeoOps("wjAppeal");
//
//        // 两个点的距离 默认单位：米
//        Distance distance = boundGeoOperations.distance("12138", "12139");
//
////        直接切换距离单位
////        Distance distance2 = boundGeoOperations.distance("12138", "12139", RedisGeoCommands.DistanceUnit.KILOMETERS);
//        System.out.println(distance);
////        输出 88.2706 METERS

    }




}
